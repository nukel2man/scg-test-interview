export const imagePlace = (photos) => {
    const config = useRuntimeConfig().public;
    let placeUrl = ``;
    if (!!photos) {
      placeUrl = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference=${photos[0].photo_reference}&key=${config.GOOGLE_MAPS_API_KEY}`;
    } else {
      placeUrl = `/no-image.jpg`;
    }
    return placeUrl;
};
