import { defineStore } from "pinia";
export const useGmapStore = defineStore({
  id: "gmap-store",
  state: () => {
    return {
      center: { lat: 13.8098659, lng: 100.5317445 },
      markers: [],
    };
  },
  actions: {
    setCenter(value) {
      this.center = value;
    },
    setMarker(value) {
      this.markers = value;
    },
  },
  getters: {},
});
