# SCG - Test Interview

By: Wiwat Krutanang (Nook)

E-mail: nukel2man@gmail.com

## Getting started

Copy file .env.dev to .env in folders "web"

Copy file .env.dev to .env in folders "api"

Add Google Map API Key in .env file in folders "web" and "api"

Go to .env file in folder `Web` add to line GOOGLE_MAPS_API_KEY = `KEY_API`

Go to .env file in folder `API` add to line GOOGLE_MAPS_API_KEY = `KEY_API`

## Run Project on Docker

Run command at root folder has a file docker-compose.yml

```bash
  docker-compose up -d
```

Then generate key for laravel run command:

```bash
docker exec interview_api php artisan key:generate
```

## Running

Front-End is running on http://localhost:3000

Back-End is running on http://localhost:8000