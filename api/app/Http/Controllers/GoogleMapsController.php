<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class GoogleMapsController extends Controller
{

    public function searchPlace(Request $request)
    {
        // Get Parametors Keyword
        $keyword = $request->get('keyword');

        // Check Cache Already Exist By Keyword
        $key = $keyword;
        if (Cache::has($key)) {
            $response = Cache::get($key);

            // Response json data
            return response()->json(['status' => true, 'data' => $response], 200);
        }


        // New keyword for search place get list from google api
        $params = [
            "type" => "restaurant",
            "query" => $keyword,
            "language" => "en",
        ];
        $response = json_decode(\GoogleMaps::load('textsearch')
            ->setParam($params)->get());

        // Cache response data from google api
        Cache::put($key, $response, now()->addHours(1));

        // Response json data
        return response()->json(['status' => true, 'data' => $response], 200);
    }
}
