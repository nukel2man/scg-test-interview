# Laravel
## Setup

Make sure to install the dependencies:

```bash
# composer
composer install

```

## Generate Key
```bash
# artisan
command: php artisan key:generate
```

## Add Key Google Map API

```bash
# .env
COPY .env.example to .env

EDIT .env

Place key googlemap api

GOOGLE_MAPS_API_KEY={key}
```


## Run
```bash
# artisan
command: php artisan serve
```
